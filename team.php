<?php
include 'check_authorization.php';

$userid = $_SESSION['user_id'];
$query = "SELECT team_name FROM freepl_teams WHERE team_user_id = '$userid'";
$result = mysql_query($query);
if(!$result){
	die("unable to query databases");
}
else{
	if(mysql_num_rows($result) == 0){
		$data['mode'] = "create";
	}
	else{
		$data['mode'] = "edit";
		$temp = mysql_fetch_assoc($result);
		$teamname = $temp['team_name'];
		$data['teamname'] = $temp['team_name'];
		$query = "SELECT player_full_name,player_country,player_role,player_id,player_cost FROM user_team_$userid,freepl_players WHERE user_flag = 1 AND user_player_id = player_id";
		
		$result = mysql_query($query);
		if(!$result){
			die("Unable to interact with database");
		}
		else{
			$currentlist = array();
			
			while($temp = mysql_fetch_assoc($result)){
				array_push($currentlist, $temp);
			}
			$data['currentlist'] = $currentlist;
		}
	}
	$query = "SELECT player_full_name,player_id,player_country,player_cost,player_role FROM freepl_players ORDER BY player_country, player_full_name ASC";
	$result = mysql_query($query);
	if(!$result){
		die("unable to interact with database");
	}
	$playerlist = array();
	while($temp = mysql_fetch_assoc($result)){
		$temp['player_full_name'] = rtrim($temp['player_full_name']);
		array_push($playerlist,$temp);
	}
	$data['playerlist'] = $playerlist;
	echo $twig->render("team.twig",$data);
}

