<?php
	include 'check_authorization.php';
	$userid = $_SESSION['user_id'];
	if((isset($_POST['players']) && isset($_POST['mode']))){
		$playersarray = array();
		foreach($_POST['players'] as $key => $playerid){
			if(array_search($playerid,$playersarray)!==false){
				die("duplicate values");
				header("Location:team.php");
			}
			else{
				array_push($playersarray, $playerid);
			}
		}
		////echo "playersarray:";
		print_r($playersarray);
		////echo "<br/>";
		$mode = $_POST['mode'];
		$len = count($playersarray);
		if($len!=11){
			header("Location:team.php");
		}
		if($mode == "create"){
			////echo "In create";
			if(!isset($_POST['team_name']) || $_POST['team_name'] == ""){
				die("Team Name not defined");
			}
			$query = "SELECT * FROM freepl_teams WHERE team_user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute($userid);
			if($stmt->rowCount() > 0){
				die("You have already created your team");
			}
			$teamname = $_POST['team_name'];
			$query = "INSERT INTO freepl_teams(team_user_id,team_name) VALUES (?,?)";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid,$teamname));
			if($stmt ->rowCount() != 1){
				die("unable to insert into database");
			}
			$query = "";
			for($i = 0; $i < 11; $i++){
				$player = $playersarray[$i];
				$costquery = "SELECT player_cost FROM freepl_players WHERE player_id = '$player'";
				$result = mysql_query($costquery);
				if(!$result){
					die("unable to fetch from database");
				}
				$temp = mysql_fetch_row($result);
				$cost += intval($temp);
				if($cost > 1000){
					die("Cost Exceeded");
				}
				$query .= "INSERT INTO user_team_$userid(user_player_id,user_flag) VALUES('$player','1');";
				////echo $query;
			}
		}
		else{
			$currentplayerlist = array();
			$query = "SELECT * FROM user_team_$userid";
			$result = mysql_query($query);
			if(!$result){
				die("unable to update databases");
			}
			else if(mysql_num_rows($result)!=0){
				while($temp = mysql_fetch_assoc($result)){
					array_push($currentplayerlist, $temp['user_player_id']);
				}
			}
			print_r($currentplayerlist);
			////echo "<br/><br/><br/>";
			$cost = 0;
			$query = "UPDATE user_team_$userid SET user_flag = 0";
			$result = mysql_query($query);
			if(!$result){
				die("unable to process requests");
			}
			$query = "";
			for($i = 0; $i < 11; $i++){
				$player = $playersarray[$i];
				$costquery = "SELECT player_cost FROM freepl_players WHERE player_id = '$player'";
				$result = mysql_query($costquery);
				if(!$result){
					die("unable to fetch from database");
				}
				$temp = mysql_fetch_row($result);
				$cost+=intval($temp[0]);
				if($cost > 1000){
					die("Cost Exceeded");
				}
				
				//echo "$i|$player<br/>";
				if(array_search($player, $currentplayerlist) !== false){
					//echo "old<br/>";
					$query.= "UPDATE user_team_$userid SET user_flag = 1 WHERE user_player_id = '$player';";
					//echo "$query<br/>";
				}
				else{
					//echo "new<br/>";
					$query.= "INSERT INTO user_team_$userid(user_player_id,user_flag) VALUES('$player','1');";
					//echo "$query<br/>";
				}
			}
			////echo "Cost:".$cost;
		}
		////echo $query;
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		$balance = 1000-$cost;
		$query = "UPDATE freepl_teams SET team_balance = '$balance' WHERE team_user_id = '$userid'";
		$result = mysql_query($query);
		if(!$result){
			die("unable to insert into database");
		}
		header("Location:team.php");
	}
	else{
		die("improper parameters passed");
	}
?>