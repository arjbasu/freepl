<?php
	if(isset($_POST['user_email'])){
		include '../connect.php';
		$email = $_POST['user_email'];
		$query = "SELECT user_id FROM freepl_users WHERE user_email = ?";
		$message['query'] = $email;
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($email));
		if($stmt->rowCount() >= 1){
			$user = $stmt->fetch();
			$user_id = $user['user_id'];
			$hash = md5($user_id.$email.date('m/d/Y h:i:s a', time()));
			$querynew = "UPDATE freepl_users SET user_password_reset=1, user_verification_id='$hash' ".
			"WHERE user_id = '$user_id'";
			$result = mysql_query($querynew);
			if(!$result){
				$status = "Error";
				$message = "Error generating password reset link";
			}
			else{
				$subject = "[FreePL] Password Reset Link";
				$content = "Your password reset link is:\n\n".
				"http://freepl.mkti.in/resetpassword.php?email=$email&hash=$hash\n\n".
				"Please visit the above link to reset your password. If you did not request for a password\n".
				"reset, then ignore this email";
				$from = "no-reply@freepl.mkti.in";
				$headers = "From: FreePL <$from> \r\n" .
				'Reply-To: no-reply@freepl.mkti.in' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
				mail($email, $subject, $content,$headers);
				$status = "success";
				$message = "Password reset link has been successfully sent to $email. Please follow the".
				" instructions provided in the mail. If you did not receive the password reset mail, please mail".
				" us your email address to freepl@mkti.in"
			}
			
		}
		else{
			$status = "Error";
			$message = "No records found";
		}
		
	}
	else{
		$status = "Error";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
?>