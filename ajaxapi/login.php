<?php
	session_start();
	include '../connect.php';
	if(isset($_POST['user_email']) && isset($_POST['user_password'])){
		if(isset($_POST['rememberme']))
			$remember = $_POST['rememberme'];
		else
			$remember = "";
		
		$email = $_POST['user_email'];
		$password = $_POST['user_password'];
		$hashedpassword = crypt($password,'$1$foreverdope12$');
		$query = "SELECT user_id,user_name,user_verified FROM freepl_users WHERE user_email = ? AND user_passhash = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($email,$hashedpassword));
		if($stmt->rowCount() == 1){
			$result = $stmt->fetch();
			$status = $result['user_verified'];
			$name = $result['user_name'];
			$user_id = $result['user_id'];
				
			if($status == 0){
				$status = "error";
				$message = "You have not yet verified your email. Please click on the link sent to your mail
				to verify your email.If you have not received the verification mail, please send your email".
				"address to freepl.mkti.in";
			}
			else{
				if($remember == "remember"){
					setcookie("user",$name,time()+14*24*60*60,"/");
					setcookie("user_id",$user_id,time()+14*24*60*60,"/");
					setcookie("user_freepl_cookie","freepl",time()+14*24*60*60);
				}
					
				$_SESSION['freepl_logged'] = true;
				$_SESSION['username'] = $name;
				$_SESSION['user_id'] = $user_id;
				$status = "success";
				$message = "You have successfully logged in. One moment...";
			}
		}
		else{
			$status = "error";
			$message = "Incorrect email/password combination";
		}
	}
	else{
		$status = "Failure";
		$message = "Improper parameters passed";
	}
	include 'json_encoding.php';
	
?>