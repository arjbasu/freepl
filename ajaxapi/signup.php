<?php
	session_start();
	include '../connect.php';
	if(isset($_POST['user_name']) && isset($_POST['user_email']) && isset($_POST['user_password'])
			&& isset($_POST['user_confirmpassword'])){
		$name = $_POST['user_name'];
		$email = $_POST['user_email'];
		$password = $_POST['user_password'];
		$confirmpassword = $_POST['user_confirmpassword'];
		$query = "SELECT * FROM freepl_users WHERE user_email = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($email));
		if($stmt->rowCount()>=1){
			$status = "Error";
			$message = "Email already exists.";
		}
		else{
			$passhash = crypt($password,'$1$foreverdope12$');
			$str = "$email$password";
			$hashvalue = md5($str);
			error_log($str,0);
			error_log($hashvalue,0);
			$stmt = $pdo->prepare("INSERT INTO freepl_users(user_name,user_email,user_passhash,user_verification_id) VALUES (?,?,?,?)");
			$stmt->execute(array($name,$email,$passhash,$hashvalue));
			if($stmt->rowCount() == 1){
				$subject = "[FreePL] Verification of account for $name";
				$message = "Congratulations!! You have succesfully registered for FreePL.\n\n".
						"Please click on the link below to verify your account:\n\n".
						"http://freepl.mkti.in/verify.php?email=".urlencode($email)."&hash=$hashvalue\n\n".
						"If you did not register for FreePL, please ignore this email";
				$from = "no-reply@freepl.mkti.in";
				$headers = "From: FreePL <$from> \r\n" .
				'Reply-To: no-reply@freepl.com' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
				mail($email, $subject, $message,$headers);
				$status = "success";
				$message = "You have successfully registered for FreePL. Please click on the link ".
				"provided in the verification mail to verify your email. If you did not receive the verification mail send us your email id to freepl@mkti.in";
			}
			else{
				$status = "error";
				$message = "Unable to insert into db";
			}
		}
		
	}
	else{
		$status = "error";
		$message = "improper parameters passed";
	}
	include 'json_encoding.php';
?>