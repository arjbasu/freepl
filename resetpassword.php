<?php
	include 'connect.php';
	include 'twiginit.php';
	if(isset($_POST['newPassword']) && isset($_POST['confirmPassword']) && isset($_POST['hash'])
			&&isset($_POST['email'])){
		$newpass = $_POST['newPassword'];
		$confirmpass = $_POST['confirmPassword'];
		if($newpass != $confirmpass){
			echo $twig->render("resetpassword.twig",array("email"=>$email,"hash"=>$hash,"update"=>true,"passmatch"=>true));
		}
		$hash = $_POST['hash'];
		$email = $_POST['email'];
		$query = "SELECT user_id FROM freepl_users WHERE user_email = ? AND user_verification_id = ? AND" 
		." user_password_reset = 1";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($email,$hash));
		if($stmt->rowCount() <1){
			die("Improper parameters passed");
		}
		else{
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$userid = $temp['user_id'];
			$passhash = crypt($newpass,'$1$foreverdope12$');
			$query = "UPDATE freepl_users SET user_passhash = ?,user_password_reset = 0 WHERE user_id = ? ";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($passhash,$userid));
			echo $twig->render("resetpassword.twig",array("success"=>true));
		}
		
	}
	else if(isset($_GET['email']) && isset($_GET['hash'])){
		$hash = $_GET['hash'];
		
		$email = $_GET['email'];
		$query = "SELECT user_id FROM freepl_users WHERE user_email = ? AND user_verification_id = ? AND "
		." user_password_reset = 1";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($email,$hash));
		if($stmt->rowCount() <1){
			die("Improper parameters passed");
		}
		else{
			echo $twig->render("resetpassword.twig",array("email"=>$email,"hash"=>$hash,"update"=>true));
		}
	} 
	else{
		die("improper paramters passed");
	}
?>