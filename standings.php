<?php
	include 'check_authorization.php';
	include 'day.php';
	$data['date'] = $date;
	$query = "SELECT team_name,day$day FROM freepl_teams ORDER BY day$day DESc";
	$result = mysql_query($query);
	if(!$result){
		die("unable to query databases");
	}
	$daily = array();
	while($temp = mysql_fetch_assoc($result)){
		$temp['score'] = $temp["day$day"];
		$temp['name'] = stripslashes($temp['team_name']);
		array_push($daily,$temp);
	}
	
	$query = "SELECT team_name,team_totalscore FROM freepl_teams ORDER BY team_totalscore DESC";
	$result = mysql_query($query);
	if(!$result){
		die("unable to query databases");
	}
	$overall = array();
	while($temp = mysql_fetch_assoc($result)){
		$temp['score'] = $temp["team_totalscore"];
		$temp['name'] = stripslashes($temp['team_name']);
		array_push($overall,$temp);
	}
	$data['daily'] = $daily;
	$data['overall'] = $overall;
	echo $twig->render("standings.twig",$data);
?>