$("document").ready(function(){
	//alert("The teams for the  match on 25th January will lock at 25th January 4:00 PM (16:00 Hrs) IST");
	$("#myCarousel").carousel(2000);
	$("body").ajaxStart(function(){
		$(".loading,.successdiv,.errordiv").hide();
	});
	$(".modal").on("hidden",function(){
		$(this).find(".loading,.successdiv,.errordiv").hide();
		$(this).find("form").show().trigger("reset");
	});
	$("#registered").click(function(){
		$("#signupmodal").modal('hide');
		$("#loginmodal").modal("show");
	});
	$("#forgotpassword").click(function(){
		$("#loginmodal").modal("hide");
		$("#resetpasswordmodal").modal("show");
	})
	$("#resetpasswordform").submit(function(){
		console.log("resetform submitted");
		var parent = "#resetpasswordmodal";
		if($(this).find("input").val() == ""){
			$(parent).find(".errordiv").html("Please complete all the fields").slideDown("200");
			return false;
		}
		data = $(this).serialize();
		$(this).slideUp().parents(parent).find(".loading").fadeIn("200");
		$.ajax({
			url:"ajaxapi/requestresetpassword.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					$(parent).find(".loading").hide();
					$(parent).find(".successdiv").html(data.message).slideDown("200");
				}
				else{
					$(parent).find(".loading").hide();
					$(parent).find(".errordiv").html(data.message).slideDown("200");
					$("#resetpasswordform").slideDown("200");
				}
			},
			error:function(){
				$(parent).find(".errordiv").html("An error occurred").slideDown("200");
				$("#resetpasswordform").slideDown("200");
			}
		})
		return false;
	})
	$("#loginform").submit(function(){
		console.log("loginform submitted");
		var parent = "#loginmodal";
		var validation = true;
		$(this).find("input").each(function(){
			if($(this).val() == ""){
				validation = false;
			}
		});
		if(validation == false){
			$(parent).find(".errordiv").html("Please complete all the fields").slideDown("200");
			return false;
		}
		else{
			$(this).slideUp("200",function(){
				$(parent).find(".loading").fadeIn("100");
				var data = $(parent).find("#loginform").serialize();
				$.ajax({
					url:"ajaxapi/login.php",
					type:"POST",
					data:data,
					dataType:"JSON",
					success:function(data){
						if(data.status == "success"){
							$(parent).find(".loading").fadeOut("100",function(){
								$(parent).find(".successdiv").html(data.message).slideDown("200");
								setTimeout(function(){window.location = "dashboard.php";},"400");
							});
						}
						else{
							$(parent).find(".loading").fadeOut("100",function(){
								$(parent).find(".errordiv").html(data.message).slideDown("200");
							});
						}
					},
					error:function(data){
						$(parent).find(".loading").fadeOut("100",function(){
							$(parent).find(".errordiv").html("An Error occurred").slideDown("200");
						});
					}
				})
			});
		}
		return false;
	});
	
	$("#signupform").submit(function(){
		console.log("signupform clicked");
		var parent = "#signupmodal";
		var validation = true;
		$(this).find("input").each(function(){
			if($(this).val() == ""){
				validation = false;
			}
		});
		if(validation == false){
			$(parent).find(".errordiv").html("Please complete all the fields").slideDown("200");
			return false;
		}
		var password = $(this).find("input[name = 'user_password']").val();
		var confirmpassword = $(this).find("input[name = 'user_confirmpassword']").val();
		if(password != confirmpassword){
			$(parent).find(".errordiv").html("Passwords do not match").slideDown("200");
			return false;
		}
		else{
			$(this).slideUp("200",function(){
				$(parent).find(".loading").fadeIn("100");
				var data = $(parent).find("#signupform").serialize();
				$.ajax({
					url:"ajaxapi/signup.php",
					type:"POST",
					data:data,
					dataType:"JSON",
					success:function(data){
						if(data.status == "success"){
							$(parent).find(".loading").fadeOut("100",function(){
								$(parent).find(".successdiv").html(data.message).slideDown("200");
							});
						}
						else{
							$(parent).find(".loading").fadeOut("100",function(){
								$(parent).find(".errordiv").html(data.message).slideDown("200");
							});
						}
					},
					error:function(data){
						$(parent).find(".loading").fadeOut("100",function(){
							$(parent).find(".errordiv").html("An Error occurred").slideDown("200");
						});
					}
				})
			});
		}
		return false;
	});
})