$("document").ready(function(){
	var cost = 0;
	var index = 0;
	$(".playernames option:selected").each(function(){
		index++;
		console.log(index);
		cost += parseInt($(this).attr("data-cost"));
		
		if(index == 11){
			if(cost > 1000){
				$("#cost span").css("color","red");
			}
			else{
				$("#cost span").css("color","#317EAC");
			}
			$("#cost span").html(cost);
		}
	});
	$("#teamform").submit(function(){
		validation = true;
		if($("#teamname").length>0){
			if($("#teamname").val() == ""){
				alert("Teamname cannot be empty");
				return false;
			}
		}
		if(cost>1000){
			alert("Cost of team exceeded budget");
			return false;
		}
		playerarray = new Array();
		index = 0;
		for(i = 0; i < 11;i++){
			value = $(".playernames").eq(i).val();
			if(value == "NULL"){
				alert("You have to choose 11 players");
				return false;
			}
			else if(playerarray.indexOf(value)!=-1){
				alert("Duplicate values");
				return false;
			}
		}
	});
	
	$(".playernames").on("change",function(){
		console.log('change');
		cost = 0;
		index = 0;
		$(".playernames option:selected").each(function(){
			index++;
			console.log(index);
			cost += parseInt($(this).attr("data-cost"));
			
			if(index == 11){
				if(cost > 1000){
					$("#cost span").css("color","red");
				}
				else{
					$("#cost span").css("color","#317EAC");
				}
				$("#cost span").html(cost);
				
			}
		});
	});
	
});